My tribute website to fulfill the [FreeCodeCamp challenge](https://www.freecodecamp.org/challenges/build-a-tribute-page).

Live website: [Charles Mingus Tribute](https://md-fcc.gitlab.io/tribute/)

This is a bare-bones HTML website, with everything contained in one HTML file. No external css or js files.

At the time I built this page, the Bootstrap framework was on version 3.3.7. I would now consider using css flexbox instead of Bootstrap because this is a simple website.